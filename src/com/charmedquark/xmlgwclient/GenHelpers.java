package com.charmedquark.xmlgwclient;

import java.util.Date;
import java.text.SimpleDateFormat;

public class GenHelpers
{
    // Convert a string to int, or return a default if not valid
    public static int textToInt(String srcVal, int defVal)
    {
    	int retVal = defVal;
    	try
    	{
    		retVal = Integer.parseInt(srcVal, 10);
    	}
    	catch(NumberFormatException nfe) {}
    	return retVal;
    }

    
    // Convert a string to float, or return a default if not valid
    public static float textToFloat(String srcVal, float defVal)
    {
    	float retVal = defVal;
    	try
    	{
    		retVal = Float.parseFloat(srcVal);
    	}
    	catch(NumberFormatException nfe) {}
    	return retVal;
    }
    
    
	//
	// 	Format a CIDLib time stamp. We get a 100-ns offset since 1970,
	//	and Java does MSs since then. So we divide by 10,000 to get the
	//	MS value, then we can use that in Java to format the time stamp.
	//
	//	We support a few formats.
	//
	public static String tmToFull(String srcStamp)
	{
		String retFmt = null;
		try
		{
			long stampVal = Long.parseLong(srcStamp, 16);
			stampVal /= 10000;

			Date tmFmt = new Date(stampVal);
			retFmt = new SimpleDateFormat("EEE, d MMM yyyy").format(tmFmt);
		}
		
    	catch(NumberFormatException nfe) {}
		
		// If not set, then set a default empty string
		if (retFmt == null)
			retFmt = new String("");
		return retFmt;
	}
    
	public GenHelpers()
	{
	}
}

