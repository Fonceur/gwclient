package com.charmedquark.xmlgwclient;

//
//	This class is used keep track of loads. We track the configuration oriented
//	data, and the indices into the poll list of the fld values associated with
//	each load (either one or two fields.)
//
//	We also provide a second set of indices for use by the activity level code
//	to use for its own purposes.
//
public class LoadInfo
{
	// Indicates the type of load involved
	public enum LoadTypes { NotUsed, Switch, Dimmer, Both };	
	
	//
	// 	This is called with the config info for a load. The second field may be
	//	null if the type isn't Both.
	//
	public LoadInfo(String name
					, LoadTypes type
					, String fldName1
					, String fldName2
					, int minVal
					, int maxVal
					, int loadIndex)
	{
		loadName = name;
		loadType = type;
		fld1Name = fldName1;
		fld2Name = fldName2;

		ex1Index = -1;
		ex2Index = -1;
		
		dimMin = minVal;
		dimMax = maxVal;
		
		// Set up our field indices based on the load index
		switch(loadIndex)
		{
			case 0 :
				fld1Index = FldInfo.FldIndices.Light_1_Fld1;
				fld2Index = FldInfo.FldIndices.Light_1_Fld2;
				break;
				
			case 1 :
				fld1Index = FldInfo.FldIndices.Light_2_Fld1;
				fld2Index = FldInfo.FldIndices.Light_2_Fld2;
				break;				

			case 2 :
				fld1Index = FldInfo.FldIndices.Light_3_Fld1;
				fld2Index = FldInfo.FldIndices.Light_3_Fld2;
				break;		
		
			case 3 :
				fld1Index = FldInfo.FldIndices.Light_4_Fld1;
				fld2Index = FldInfo.FldIndices.Light_4_Fld2;
				break;
				
			default :
				fld1Index = FldInfo.FldIndices.FldCount;
				fld2Index = FldInfo.FldIndices.FldCount;
				break;
		};
		
		//
		// 	If it's not a Both type, then trash the 2nd field index,
		//	since it's only valid in this case.
		//
		if (type != LoadTypes.Both)
			fld2Index = FldInfo.FldIndices.FldCount;
	}

	public LoadInfo(LoadInfo toCopy)
	{
		loadName = toCopy.loadName;
		loadType = toCopy.loadType;
		fld1Index = toCopy.fld1Index;
		fld2Index = toCopy.fld2Index;
		fld1Name = toCopy.fld1Name;
		fld2Name = toCopy.fld2Name;
		
		ex1Index = toCopy.ex1Index;
		ex2Index = toCopy.ex2Index;
		
		dimMin = toCopy.dimMin;
		dimMax = toCopy.dimMax;
	}
	
	public String  loadName;
	public LoadTypes loadType;
	public String fld1Name;
	public String fld2Name;
	
	// If the second one isn'tused it'll be set to FldCount
	public FldInfo.FldIndices fld1Index;
	public FldInfo.FldIndices fld2Index;

	public int dimMin;
	public int dimMax;
	
	public int ex1Index;
	public int ex2Index;
}
