package com.charmedquark.xmlgwclient;

//
//	For any of the field based info we keep track of, we need to keep up
//	with various bits of info. For any field we will need to track the
//	field's name, a good value/in error flag, and a value if the value
//	is good.
//
//	We also keep a value serial number that is updated each time the
//	value is changed, which is used to know if visual elements related to
//	the field need to update.
//
//	We also provide some convenient methods to get the value as various
//	types.
//
public class FldInfo
{
	//
	//	Since we deal with a known set of fields, and since we want to have
	//	some way for the derived classes to efficiently link themselves to
	//	the fields they care about without a lot of complexity, we just create
	//	an enum that reflects all possible fields. We can then create an
	//	array of ints, each of which is the index of that field into the 
	//	poll list we use to get info from the server.
	//
	//	This way derived classes can just ask for a field by the enum. We
	//	go to enum indexed slot to find the index into the poll list, and we
	//	have the field info for that field.
	//
	//	If fields aren't used, they are set to -1 to indicate this.
	//
	public enum FldIndices
	{
		// Lighting oriented
		Light_1_Fld1
		, Light_1_Fld2
		, Light_2_Fld1
		, Light_2_Fld2
		, Light_3_Fld1
		, Light_3_Fld2
		, Light_4_Fld1
		, Light_4_Fld2		
		
		// Weather current condition related
		, Weath_CurBaro
		, Weath_CurCondText
		, Weath_CurFCText
		, Weath_CurHumidity
		, Weath_CurIcon
		, Weath_CurTemp
		, Weath_CurWindSpeed
		
		// Weather FC day 1
		, Weath_FCD1_Stamp
		, Weath_FCD1_Icon
		, Weath_FCD1_CondText
		, Weath_FCD1_High
		, Weath_FCD1_Low
		
		// Weather FC day 2
		, Weath_FCD2_Stamp
		, Weath_FCD2_Icon
		, Weath_FCD2_CondText
		, Weath_FCD2_High
		, Weath_FCD2_Low
		
		// Weather FC day 3
		, Weath_FCD3_Stamp
		, Weath_FCD3_Icon
		, Weath_FCD3_CondText
		, Weath_FCD3_High
		, Weath_FCD3_Low
		
		// The ordinal of this one is the count of fields
		, FldCount
	};
	
	
	public FldInfo(String name)
	{
		fldName = name;
		fldValue = new String();
		goodValue = false;
		valSerialNum = 0;
	}
	
	public FldInfo(FldInfo toCopy)
	{
		fldName = toCopy.fldName;
		fldValue = toCopy.fldValue;
		goodValue = toCopy.goodValue;
		valSerialNum = toCopy.valSerialNum;
	}

	public boolean getBoolVal()
	{
		return (fldValue.equals("True"));
	}

	// Return our value as an int, providing a default if not valid/available	
	public int getIntVal(int defVal)
	{
		return GenHelpers.textToInt(fldValue, defVal);
	}

	// Return our value as a float, providing a default if not valid/available
	public float getFloatVal(float defVal)
	{
		return GenHelpers.textToFloat(fldValue, defVal);
	}

	//
	//	Get a a float formatted to a string with 2 decimals, or an empty
	//	string if not valid.
	//
	public String getFmtFloatVal()
	{
    	String retVal = null;
    	try
    	{
    		float fmtVal = Float.parseFloat(fldValue);
    		retVal = String.format("%.2f", fmtVal);
    	}
    	catch(NumberFormatException nfe) {}
    	
    	if (retVal == null)
    		retVal = "";
		 return retVal;
	}
	
	
	public String getStringVal()
	{
		return fldValue;
	}

	public void setValue(String newValue)
	{
		 if (!newValue.equals(fldValue))
		 {
			 // The value changed and that means we have a good value
			 fldValue = newValue;
			 goodValue = true;
			 valSerialNum++;
		 }
		  else if (!goodValue)
		 {
			  // Value is the same, but we have a good value now
			  goodValue = true;
			  valSerialNum++;
		 }
	}
	
	// Set us into error state, if not already
	public void setErrState()
	{
		if (goodValue)
		{
			goodValue = false;
			valSerialNum++;			
		}
	}
	
	public String fldName;
    public int valSerialNum;
	
	private boolean goodValue;
	private String fldValue;
}
