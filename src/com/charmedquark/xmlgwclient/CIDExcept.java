package com.charmedquark.xmlgwclient;

public final class CIDExcept extends RuntimeException
{
    public static final long serialVersionUID = 1;
    
	public CIDExcept()
	{
	}

	public CIDExcept(String errtext, String src, int line, String process, String thread)
	{
		super(errtext);
		line = lineNum;
		procName = process;
		srcFile = src;
		thrName = thread;
	}

	public CIDExcept(Throwable throwable, String src, int line, String process, String thread)
	{
		super(throwable);
		line = lineNum;
		procName = process;
		srcFile = src;
		thrName = thread;		
	}

	public CIDExcept(String detailMessage
					, Throwable throwable
					, String src
					, int line
					, String process
					, String thread)
	{
		super(detailMessage, throwable);

		line = lineNum;
		procName = process;
		srcFile = src;
		thrName = thread;
	}

	public int lineNum;
	public String procName;
	public String srcFile;
	public String thrName;
}
