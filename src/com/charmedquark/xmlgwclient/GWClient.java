package com.charmedquark.xmlgwclient;

// We need the XML parser and I/O stuff, and crypto for login
import java.io.*;
import java.net.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import org.w3c.dom.*;

import java.security.*;
import java.util.Vector;

import javax.crypto.*;
import javax.crypto.spec.*;

//
//	This class provides the connection to the XML GW server, and provides the
// 	basic methods for logging in, logging out, and sending and receiving
//	XML messages. When receiving, the XML will have been parsed and known
//	correct, so the caller gets the parsed representation.
//
public final class GWClient
{
	// The caller has to provide the username, password, and GW server address
	public GWClient()
	{
		srvSock = null;

		//
		// 	Allocate some read/write buffers. These are for reading chunks at
		//	at a time, so we can create them at a specific max size.
		//
		inBuf = new byte[inBufSz];

		// Allocate our XML parser object
		try
		{
			DocumentBuilderFactory docBldFact = DocumentBuilderFactory.newInstance();
			docBldFact.setValidating(false);
			docBuilder = docBldFact.newDocumentBuilder();
		}
		
		catch(ParserConfigurationException e)
		{
			throw new RuntimeException("Msg parse failed - Parser config error", e);
		}
	}


	// -----------------------------------------------------------------------
	//	Public, static helper methods that will be used by the higher level
	//	code, mostly related to XML msg parsing, building, checking, etc...
	// -----------------------------------------------------------------------	
	
	//
	//	A lot of messages are quite simple, often just a msg element with no or
	//	a small number of attributes. These are some helpers to build those up
	//	easily.
	//
	//	We assume here that any attribute values are pre-escaped if needed, so that
	//	code that knows it doesn't require it won't pay the cost of doing it.
	//
	public static String buildSimpleMsg(String msgType)
	{
		StringBuilder sbTar = new StringBuilder();
		
		fmtMsgHeader(sbTar);
		sbTar.append("   <");
		sbTar.append(msgType);
		sbTar.append("/>");
		fmtMsgFooter(sbTar);
		
		return sbTar.toString(); 
	}
	
	public static String buildSimpleMsg(String msgType, String attr1, String attrVal)
	{
		StringBuilder sbTar = new StringBuilder();
		
		fmtMsgHeader(sbTar);
		sbTar.append("   <");
		sbTar.append(msgType);
		sbTar.append(" ");
		sbTar.append(attr1);
		sbTar.append("=\"");
		sbTar.append(attrVal);
		sbTar.append("\"/>");
		fmtMsgFooter(sbTar);
		
		return sbTar.toString();
	}


	public static String
	buildSimpleMsg(	String msgType, String attr1, String attrVal
					, String attr2, String attrVal2)
	{
		StringBuilder sbTar = new StringBuilder();
		
		fmtMsgHeader(sbTar);
		sbTar.append("   <");
		sbTar.append(msgType);
		sbTar.append(" ");
		sbTar.append(attr1);
		sbTar.append("=\"");
		sbTar.append(attrVal);
		sbTar.append("\" ");
		sbTar.append(attr2);
		sbTar.append("=\"");
		sbTar.append(attrVal2);
		sbTar.append("\"/>");
		fmtMsgFooter(sbTar);

		return sbTar.toString();
	}


	public static String buildSimpleMsg(String msgType
										, String attr1, String attrVal
										, String attr2, String attrVal2
										, String attr3, String attrVal3)
	{
		StringBuilder sbTar = new StringBuilder();

		fmtMsgHeader(sbTar);
		sbTar.append("   <");
		sbTar.append(msgType);
		sbTar.append(" ");
		sbTar.append(attr1);
		sbTar.append("=\"");
		sbTar.append(attrVal);
		sbTar.append("\" ");
		sbTar.append(attr2);
		sbTar.append("=\"");
		sbTar.append(attrVal2);
		sbTar.append("\" ");
		sbTar.append(attr3);
		sbTar.append("=\"");
		sbTar.append(attrVal3);
		sbTar.append("\"/>");
		fmtMsgFooter(sbTar);
		
		return sbTar.toString();
	}
	
	public static String buildSimpleMsg(String msgType
			, String attr1, String attrVal
			, String attr2, String attrVal2
			, String attr3, String attrVal3
			, String attr4, String attrVal4)
	{
		StringBuilder sbTar = new StringBuilder();
		
		fmtMsgHeader(sbTar);
		sbTar.append("   <");
		sbTar.append(msgType);
		sbTar.append(" ");
		sbTar.append(attr1);
		sbTar.append("=\"");
		sbTar.append(attrVal);
		sbTar.append("\" ");
		sbTar.append(attr2);
		sbTar.append("=\"");
		sbTar.append(attrVal2);
		sbTar.append("\" ");
		sbTar.append(attr3);
		sbTar.append("=\"");
		sbTar.append(attrVal3);
		sbTar.append("\" ");
		sbTar.append(attr4);
		sbTar.append("=\"");
		sbTar.append(attrVal4);
		sbTar.append("\"/>");
		fmtMsgFooter(sbTar);
		
		return sbTar.toString();
	}	

	// Check an attribute of the passed element to see if it has a specific value
	public static Boolean
	checkAttrVal(Element elemCheck, String attrName, String expectedVal)
	{
		String attrVal = elemCheck.getAttribute(attrName); 
		return attrVal.equals(expectedVal);
	}
	
	
	//
	// 	Check the type of an incoming msg and the contained msg specific node and 
	//	throw if not. An alternative type can be provided as well.
	//
	public static Element
	checkMsgType(Document docCheck, String expectedType, String altType)
	{
		// Make sure the root is an XML GW message
		Element rootElem = docCheck.getDocumentElement();
		if (!rootElem.getNodeName().equals("CQCGW:Msg"))
		{
			String errMsg = "Msg was not a CQCGW XML msg. Type=";
			errMsg += rootElem.getNodeName();
			throw new RuntimeException(errMsg);		
		}

		//
		// 	Get the first element child of the root, which should be the
		//	actual msg.
		//
		Element msgElem = null;
		{
			Node curNode = rootElem.getFirstChild();
			while (curNode != null)
			{
				if (curNode.getNodeType() == Node.ELEMENT_NODE)
				{
					msgElem = (Element)curNode;
					break;
				}
				curNode = curNode.getNextSibling();
			}
		}
		
		if (msgElem == null)
			throw new RuntimeException("The msg had no msg type element");

		// If an expected type, then check it
		if (expectedType != null)
		{
			if (!msgElem.getNodeName().equals(expectedType))
			{
				if ((altType == null) || !msgElem.getNodeName().equals(altType)) 
				{		
					// If it's an exception msg, then throw it
					if (msgElem.getNodeName().equals("CQCGW:ExceptionReply"))
						throwCIDErr(msgElem);
				
					String errMsg = "Expected msg type '";
					errMsg += expectedType;
					errMsg += "' but got '";
					errMsg += msgElem.getNodeName();
					errMsg += "'";
					throw new RuntimeException(errMsg);
				}
			}
		}
		return msgElem;
	}
	
	
	// Check the type of an incoming node and throw if not
	public static void checkNodeType(Element elemCheck, String expectedType)
	{
		if (!elemCheck.getNodeName().equals(expectedType))
		{
			String errMsg = "Expected msg type '";
			errMsg += expectedType;
			errMsg += "' but got '";
			errMsg += elemCheck.getNodeName();
			errMsg += "'";
			throw new RuntimeException(errMsg);
		}
	}

	public static Element
	findFirstNamedElem(Element parent, String toFind, boolean throwIfNot)
	{
		// Go through and pull the info out
		Element elemRet = null;
		Node curNode = parent.getFirstChild();
		while (curNode != null)
		{
			if (curNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element elemTest = (Element)curNode;
				if (elemTest.getNodeName().equals(toFind))
				{
					elemRet = elemTest;
					break;
				}
			}
			curNode = curNode.getNextSibling();
		}
		
		if (throwIfNot && (elemRet == null))
			throw new RuntimeException("No node named '" + toFind + "' was found");

		return elemRet;
	}

	
	// Add a standard message footer to the passed string builder
	public static void fmtMsgFooter(StringBuilder sbTar)
	{
		sbTar.append("\n</CQCGW:Msg>\n");
	}

	
	// Build up a standard message hader in the target string builder
	public static void fmtMsgHeader(StringBuilder sbTar)
	{
		sbTar.append("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");
		sbTar.append("<!DOCTYPE CQCGW:Msg PUBLIC 'urn:charmedquark.com:CQC-GWProtocol.DTD' 'CQCGWProtocol.DTD'>\n");
		sbTar.append("<CQCGW:Msg>\n");
	}

	//
	//	Get the text content of an element. Instead of calling 
	//	getTextContent() on the element, which can get white space
	//	we don't want, we find the text node and get his value.
	//
	public static String getMsgTextContent(Element parent)
	{
		Node curNode = parent.getFirstChild();
		while (curNode != null)
		{
			if (curNode.getNodeType() == Node.TEXT_NODE)
			{
				Text textNode = (Text)curNode;
				return textNode.getData().replaceAll("^\\s+|\\s+$", "");
			}
			curNode = curNode.getNextSibling();
		}
		return "";
	}
		
	//
	//	There's a very common scenario where a query is done, and it returns
	//	a set of parallel lists, and we want to pull the text out of each of
	//	lists at a given index and add them to a vector of key/value pairs.
	//	This can end up being a lot of grunt code, so we provide the helper
	// 	to do this work.
	//
	//	We get the parent element of the lists, a vector of k/v pairs to
	//	fill in, then up to three child list element names. The values in
	//	the first one goes into the key, and the other two go into the first
	//	and second value. The second one can be null if it's not needed.
	//
	//	We always add to the list, so we can be called repeatedly to load
	//	up multiple chunks.
	//
	//	All of the lists MUST be the same size. They are assumed to be
	//	parallel lists.
	//
	public static int
	parseKVLists(Element parElem
				, Vector<KeyValuePair> toFill
				, String keyList
				, String val1List
				, String val2List)
	{
		//
		// 	The first pass must create the k/v pairs. We'll go back in subsequent
		//	passes and update the ones we create in this first pass. Remember the
		//	current count so that we can do that.
		//
		final int initIndex = toFill.size();
		
		Element listElem = findFirstNamedElem(parElem, keyList, true);
		Node curNode = listElem.getFirstChild();
		int addedCnt = 0;
		while (curNode != null)
		{
			if (curNode.getNodeType() == Node.ELEMENT_NODE)
			{
				toFill.add(new KeyValuePair(((Element)curNode).getTextContent()));
				addedCnt++;
			}
			curNode = curNode.getNextSibling();					
		}
		
		// Now do the the first value list if present
		if (val1List != null)
		{
			listElem = findFirstNamedElem(parElem, val1List, true);
			curNode = listElem.getFirstChild();
			int index = initIndex;
			while (curNode != null)
			{
				if (curNode.getNodeType() == Node.ELEMENT_NODE)
				{
					toFill.get(index).setValue(((Element)curNode).getTextContent());
					index++;
				}
				curNode = curNode.getNextSibling();					
			}
		}
		
		// And the second value list if present
		if (val2List != null)
		{
			listElem = findFirstNamedElem(parElem, val2List, true);
			curNode = listElem.getFirstChild();
			int index = initIndex;
			while (curNode != null)
			{
				if (curNode.getNodeType() == Node.ELEMENT_NODE)
				{
					toFill.get(index).setValue2(((Element)curNode).getTextContent());
					index++;
				}
				curNode = curNode.getNextSibling();					
			}
		}
		
		// Return the count of items we added
		return addedCnt;
	}
	
	
	// -----------------------------------------------------------------------
	//	Public helper methods, mostly related to maintaining and checking
	//	the connection to the XMLGW.
	// -----------------------------------------------------------------------
	
	// Check to see if we have the socket, i.e. we seem connected
	public Boolean checkIsConnected()
	{
		return (srvSock != null);
	}
	
	
	//
	//	If not already connected, try to connect. The caller has to provide
	//	the username, password, and GW server address.
	//
	public void
	connect(String name, String pword, String addr, int port)
	{
		// If connected, then disconnect and reconnect with the new info
		if (srvSock != null)
			disconnect();

		// Store the new info away
		password = pword;
		srvPort = port;
		srvAddr = addr;
		userName = name;
		
		// Try to open a socket connection to the server
		try
		{
			srvSock = new Socket();

			InetSocketAddress tarSrvAddr = new InetSocketAddress(srvAddr, srvPort);
			srvSock.connect(tarSrvAddr, 2500);

			//
			// 	It worked, so reset any per-connect stuff. The seq numbers
			//	really star at 1, but the initial message that comes in
			//	upon connect doesn't use the sequence number, but is effectively
			//	zero, so starting the input number at zero lets us avoid a
			//	special case for that message.
			//
//			seqNumIn = 0;
			seqNumOut = 1;
			
			// Now we need to do the login sequence
			doLogin();
		}
		
		catch(UnknownHostException e)
		{
			disconnect();
			srvSock = null;
			throw new RuntimeException("Connect failed - can't find host", e);
		}
		
		catch(IOException e)
		{
			disconnect();
			srvSock = null;
			throw new RuntimeException("Connect failed - socket error", e);
		}
	}


	// Disconnect if we are connected
	public void disconnect()
	{
		if (srvSock != null)
		{
			try
			{
				srvSock.close();
				srvSock = null;
			}
			
			catch(IOException e)
			{
			}
			
			password = new String();
			srvPort = 0;
			srvAddr = new String();
			userName = new String();
		}
	}

	


	//
	//	We read in a message and verify that it's a valid XML message. We return
	//	the parsed XML document.
	//
	//	We first issue a read for a header's worth of bytes. We verify that the
	//	header is valid. If so, we issue a read for the reported payload size.
	//	If we get that data, then we try to parse it.
	//
	//	We have an internal helper to read specific counts of bytes, which may
	//	not all show up at once.
	//
	private void readBytes(	InputStream srcStrm
							, byte toFill[]
							, int toRead
							, String contDesc) throws IOException
	{
		int readCnt = 0;
		while (readCnt < toRead)
		{
			int curCnt = srcStrm.read(inBuf, readCnt, toRead - readCnt);
			if (curCnt < 0)
			{
				disconnect();
				throw new RuntimeException("Failed to read " + contDesc);
			}
			readCnt += curCnt;
		}
	}
	
	public Document readMsg()
	{
		int readCnt, curCnt, checkSum, checkSum2;
		long magicVal1, magicVal2, payloadSz; // , seqNum;
		Document doc = null;		
		try
		{
			InputStream inStrm = srvSock.getInputStream();

			// Read the header part
			readBytes(inStrm, inBuf, 19, "msg header");

			// And the second magic value
			magicVal2 = extractC4Value(inBuf, 15);
			if (magicVal2 != 4276993709L)
				throw new RuntimeException("Invalid magic hdr value 2");		

			// Test the first magic value
			magicVal1 = extractC4Value(inBuf, 0);
			if (magicVal1 != 3735928559L)
				throw new RuntimeException("Invalid magic hdr value 1");		
		
			// Check the sequence number, and make sure it's good
//			seqNum = extractC4Value(inBuf, 4);
//			if (seqNum != seqNumIn)
//				throw new RuntimeException("Got invalid sequence number");
			
			// Bump the expected incoming count for the next round
//			seqNumIn++;
	
			// Get the payload size
			payloadSz = extractC4Value(inBuf, 8);
			
			// Get the check sum
			checkSum = extractC2Value(inBuf,  12);

			//
			// 	We have gotten all the header info out, so we can reuse the in
			//	buffer for the payload.	See if we need to reallocate it.
			//
			if (inBufSz < payloadSz)
			{
				// Round it to the next K
				inBufSz = (((int)payloadSz / 1024) + 1) * 1024;
				inBuf = new byte[inBufSz];
			}

			readCnt = (int)payloadSz;
			readBytes(inStrm, inBuf, readCnt, "payload bytes");

			// Calculate the check sum of the buffer
			checkSum2 = calcSum(inBuf, readCnt);
			if (checkSum != checkSum2)
				throw new RuntimeException("Invalid check sum on incoming buffer");
	
			// Set up the message buffer so we can feed it into the XML parser
			ByteArrayInputStream strmMsg = new ByteArrayInputStream(inBuf, 0, readCnt);

			// Reset our document builder and parse this msg
			docBuilder.reset();
			doc = docBuilder.parse(strmMsg);
		}

		catch(SAXException e)
		{
			throw new RuntimeException("Msg parse failed - SAX error", e);
		}

		catch(IOException e)
		{
			throw new RuntimeException("Msg parse failed - IO Error", e);
		}
		
		// It was parsed ok, so return it
		return doc;
	}

	//
	// 	Send the passed XML message to the server. We format the text to
	//	binary and set up a header for the message with the binary payload
	//	info, then we send the two parts.
	//
	public void sendMsg(String msgText)
	{
		//
		// 	Format out the message text using UTF-8. We have to get it into
		//	a buffer so that we know how many bytes are in it.
		//
		byte[] msgBytes = null;
		try
		{
			msgBytes = msgText.getBytes("UTF-8");
		}
		
		catch(IOException e)
		{
			throw new RuntimeException("UTF-8 conversion failed", e);
		}
		
		// Now let's set up the header required 
		ByteArrayOutputStream strmHdr = new ByteArrayOutputStream();
		
		// Write the first magic value (little endian)
		strmHdr.write(0xEF);
		strmHdr.write(0xBE);
		strmHdr.write(0xAD);
		strmHdr.write(0xDE);
		
		// Write the sequence number (little endian)
		strmHdr.write((byte)(seqNumOut & 0xFF));
		strmHdr.write((byte)((seqNumOut >> 8) & 0xFF));
		strmHdr.write((byte)((seqNumOut >> 16) & 0xFF));
		strmHdr.write((byte)((seqNumOut >> 24) & 0xFF));
		
		// Write out the data size
		int msgLen = msgBytes.length;
		strmHdr.write((byte)(msgLen & 0xFF));
		strmHdr.write((byte)((msgLen >> 8) & 0xFF));
		strmHdr.write((byte)((msgLen >> 16) & 0xFF));
		strmHdr.write((byte)((msgLen >> 24) & 0xFF));

		// Calc the check sum and write it out
		int checkSum = calcSum(msgBytes, msgLen);
		strmHdr.write((byte)(checkSum & 0xFF));
		strmHdr.write((byte)((checkSum >> 8) & 0xFF));
		
		// Zero flags byte
		strmHdr.write(0);
		
		// Write the closing magic value
		strmHdr.write(0xAD);
		strmHdr.write(0xBE);
		strmHdr.write(0xED);
		strmHdr.write(0xFE);
		
		// This should come out to be 19 bytes
		try
		{
			strmHdr.flush();
			int hdrLen = strmHdr.size();
			byte[] hdrBytes = strmHdr.toByteArray();
			
			// Get the socket's output stream
			OutputStream outStrm = srvSock.getOutputStream();
			
			// Send the header and then the message text
			outStrm.write(hdrBytes, 0, hdrLen);
			outStrm.write(msgBytes, 0, msgLen);
		}
		
		catch(IOException e)
		{
			throw new RuntimeException("Couldn't transmit message", e);
		}

		//
		// 	It worked so bump the output sequence, wrapping if needed. We don't
		//	need to handle making sure it never goes to zero, since we are using
		//	a long in order to handle 32 unsigned bits. So we just watch for
		//	exceeding the 32 bit range and wrap back to 1.
		//
		seqNumOut++;
		if (seqNumOut > 0xFFFFFFFF)
			seqNumOut = 1;
	}
	
	
	//
	//	At various times an ack is expected, else it's a nak which means some
	//	operation was rejected, or perhaps of course an exception.
	//
	public void waitAck()
	{
		Document doc = readMsg();
		Element elemMsg = checkMsgType(doc, "CQCGW:AckReply", "CQCGW:NakReply");
		
		// It's one of the two. If a nak, then throw
		if (elemMsg.getNodeName().equals("CQCGW:NakReply"))
			throw new RuntimeException(elemMsg.getAttribute("CQCGW:ReasonText"));
	}
	

	// ------------------------------------------------------------------------
	// 	These are higher level methods that read messages and parse them on
	//	behalf of the caller, and return the data in more useable format.
	// ------------------------------------------------------------------------	

	// Does a simple ping operation
	public void ping()
	{
		String msgOut = buildSimpleMsg("CQCGW:Query", "CQCGW:QueryType", "CQCGW:Ping");
		sendMsg(msgOut);
		waitAck();
	}

	
	//
	//	Once a poll list has been set, poll the list and get back any
	//	changed fields.
	//
	public Boolean pollFields(Vector<FldPollStatus> changedFlds)
	{
		changedFlds.clear();
		
		String msgOut = buildSimpleMsg("CQCGW:Query", "CQCGW:QueryType", "CQCGW:Poll");
		sendMsg(msgOut);

		// Wait for a reply, which should be a poll reply
		Document doc = readMsg();
		Element msgElem = checkMsgType(doc, "CQCGW:PollReply", null);		

		// Go throw the child elements, each of which is a FldValue element
		Node curNode = msgElem.getFirstChild();
		while (curNode != null)
		{
			if (curNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element curElem = (Element)curNode;
				String curName = curElem.getAttribute("CQCGW:Field");
				String curStatus = curElem.getAttribute("CQCGW:Status");
				
				// Get the index of this guy in the original poll list
				int curIndex = GenHelpers.textToInt(curElem.getAttribute("CQCGW:Index"), -1);
				
				if (curStatus.equals("CQCGW:Online"))
				{
					changedFlds.add
					(
						new FldPollStatus
						(
							curName, curIndex, curElem.getAttribute("CQCGW:Value")
						)
					);
				}
			  	 else
				{
			  		 // This ctor will set the status to error for us
			  		changedFlds.add(new FldPollStatus(curName, curIndex));		  		 
				}
			}
			curNode = curNode.getNextSibling();
		}		
		
		// Return whether we are returning any changes
		return !changedFlds.isEmpty();
	}

	
	//
	// Query a list of available configured rooms
	//
	public Boolean queryConfiguredRooms(Vector<String> roomList)
	{
		String msgOut = buildSimpleMsg("CQCGW:Query", "CQCGW:QueryType", "CQCGW:QueryRmCfgList");
		sendMsg(msgOut);

		// Wait for a reply, which should be a room list message
		Document doc = readMsg();
		Element msgElem = checkMsgType(doc, "CQCGW:RoomList", null);

		// Clear the return list in case we don't add any
		roomList.clear();
		
		// Iterate the child elements and get the name attribute of each one
		Node curNode = msgElem.getFirstChild();
		while (curNode != null)
		{
			if (curNode.getNodeType() == Node.ELEMENT_NODE)
			{
				String curRoom = ((Element)curNode).getAttribute("CQCGW:Name");
				roomList.add(curRoom);
			}
			curNode = curNode.getNextSibling();
		}

		return (roomList.size() != 0);
	}
	
	

	//
	// 	Read a value from a field. Once version takes separate device
	//	and field names and just calls theo ther with the combined name.
	//
	public String readField(String devName, String fldName)
	{
		String srcFld = new String(devName + "." + fldName);
		return readField(srcFld);
	}
	
	public String readField(String fldName)
	{
		String msgOut = buildSimpleMsg
		(
			"CQCGW:ReadField", "CQCGW:Field", fldName
		);
		sendMsg(msgOut);

		// Wait for the reply, which should be a field value msg
		Document doc = readMsg();
		Element msgElem = checkMsgType(doc, "CQCGW:FldValue", null);
		
		if (!msgElem.getAttribute("CQCGW:Statys").equals("CQCGW:Online"))
			return null;
			
		return msgElem.getAttribute("CQCGW:Value");
	}


	//
	// Set a new poll list (or clear it by passing a null)
	//
	public void setPollList(Vector<String> fldList)
	{
		StringBuilder sbTar = new StringBuilder();
		fmtMsgHeader(sbTar);

		//
		// 	Put the list of fields within the poll list. If a null list
		//	we just don't output any.
		//
		sbTar.append("   <CQCGW:SetPollList>\n");
		if (fldList != null)
		{
			int fldCnt = fldList.size();
			for (int index = 0; index < fldCnt; index++)
			{
				String curFld = fldList.get(index);
				sbTar.append("      <CQCGW:FldName CQCGW:Name=\"");
				sbTar.append(curFld);
				sbTar.append("\"/>\n");			
			}
		}
		sbTar.append("   </CQCGW:SetPollList>\n");
		fmtMsgFooter(sbTar);
		
		// Send and wait for an ack or nak
		sendMsg(sbTar.toString());
		waitAck();
	}


	// ------------------------------------------------------------------------
	//	Private method
	// ------------------------------------------------------------------------

	// Convert a buffer toa string of hex character pairs
	private static String bufToHexStr(byte[] bytes)
	{
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for (int j = 0; j < bytes.length; j++)
	    {
	        v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	// Calc the check sum for the passed message bytes
	private static int calcSum(byte[] msgBytes, int msgLen)
	{
		int curInt;
		int sum = 0;
		int index = 0;
		while (index < msgLen)
		{
			byte curByte = msgBytes[index];
			if ((curByte & 0x80) != 0)
			{
				curByte &= 0x7F;
				curInt = curByte;
				curInt |= 0x80;
				sum += curInt; 
			}
			 else
			{
				 sum += (int)curByte;
			}
			index++;
		}
		
		// Clip it back to 16 bits
		return (sum & 0xFFFF);
	}
	
	
	// Try the login sequence
	private void doLogin()
	{
		// We should immediately get a ConnRes response
		try
		{
			Document doc = readMsg();
			Element msgElem = checkMsgType(doc, "CQCGW:ConnRes", null);
			if (!checkAttrVal(msgElem, "CQCGW:ConnStatus", "CQCGW:Accepted"))
				throw new RuntimeException("Server did not accept connection");
			
			// Now we need to build a logon request and send it
			String msgOut = buildSimpleMsg("CQCGW:LogonReq", "CQCGW:UserName", userName);
			sendMsg(msgOut);
			
			// And we should get a logon challenge back
			doc = readMsg();
			msgElem = checkMsgType(doc, "CQCGW:LogonChallenge", null);
			
			String challengeKey = msgElem.getAttribute("CQCGW:ChallengeKey");
			String challengeData = msgElem.getAttribute("CQCGW:ChallengeData");
			
			// The key and data must be 32 characters
			if ((challengeKey.length() != 32) || (challengeData.length() != 32))
				throw new RuntimeException("Invalid challenge data");

			//
			//	Do the encryption stuff required to answer the challenge. 
			//	Note that everything we deal with here is in even multiples
			//	of the needed block size for the Blowfish encrypter. It's 
			//	all MD5 hashes basically.
			//
			MessageDigest md = null;
				
			byte[] bufTmp = new byte[16];
			md = MessageDigest.getInstance("MD5");
		
			// Create a hash of the password bytes
			byte[] bufChalData = null;
			byte[] bufKeyData = null;
			{
				Cipher cipher = Cipher.getInstance("Blowfish/ECB/NoPadding");
				
				// Create a hash of the password bytes
				byte[] pwHash = md.digest(password.getBytes("UTF-8"));

				// Create a key with the hash
				SecretKeySpec pwKey = new SecretKeySpec(pwHash, "Blowfish");
				cipher.init(Cipher.DECRYPT_MODE, pwKey);
			
				// Convert the data string to a byte array and decrypt
				stringToBuf(challengeData, bufTmp);
				bufChalData = cipher.doFinal(bufTmp);
			
				//	Do the same thing for the key data
				stringToBuf(challengeKey, bufTmp);
				bufKeyData = cipher.doFinal(bufTmp);
			}
			
			//
			// 	Now create a new cipher for encrypt mode, and now the key
			//	is the decrypted challenge key.
			//
			String retStr = null;
			{
				Cipher cipher = Cipher.getInstance("Blowfish/ECB/NoPadding");
				SecretKeySpec pwKey = new SecretKeySpec(bufKeyData, "Blowfish");
				cipher.init(Cipher.ENCRYPT_MODE, pwKey);
				
				// Encrypt the challenge data now with this key
				byte[] bufChalRep = cipher.doFinal(bufChalData);
				
				// And format it back out
				retStr = bufToHexStr(bufChalRep);	
			}

			// OK, we can send the response
			msgOut = buildSimpleMsg("CQCGW:GetSecurityToken", "CQCGW:ResponseData", retStr);
			sendMsg(msgOut);

			//
			// 	And we should get an ack back. Because we want to keep things
			//	simple and put the sequence number check and increment into
			//	the message reader, we have to reset the expected value  to
			//	one here since this is really where the sequence starts.
			//
//			seqNumIn = 1;
			waitAck();
		}
		
		catch(InvalidKeyException e)
		{
			throw new RuntimeException("Invalid Blowfish key", e);
		}
		
		catch(IllegalBlockSizeException e)
		{
			throw new RuntimeException("Invalid crypto block size", e);
		}
		
		catch(NoSuchAlgorithmException e)
		{
			throw new RuntimeException("MD5 not supported", e);
		}
		
		catch(NoSuchPaddingException e)
		{
			throw new RuntimeException("Padding type not supported", e);
		}

		catch(BadPaddingException e)
		{
			throw new RuntimeException("Bad crypto padding", e);
		}
		
		catch(UnsupportedEncodingException e)
		{
			throw new RuntimeException("Can't convert password to UTF-8", e);
		}
	}

	//
	// 	Extract a unsigned values from the buffer at the indicated index,
	//	byte flipped since the values are little endian in the buffer.
	//
	private static int extractC2Value(byte[] bufBytes, int startInd)
	{
		int iRet = 0;
		int iCur;
		
		iCur = (char)bufBytes[startInd + 1];
		if (iCur > 127)
		{
			iRet = iCur & 0x7F;
			iRet += 0x80;
		}
		 else
		{
			 iRet = iCur;
		}
 		iRet *= 256;
 		
		iCur = (char)bufBytes[startInd];
		if (iCur > 127)
		{
			iRet += iCur & 0x7F;
			iRet += 0x80;
		}
		 else
		{
			 iRet += iCur;
		}
		return iRet;
	}
	
	private static long extractC4Value(byte[] bufBytes, int startInd)
	{
		long lRet = 0;
		int iCur;
		
		iCur = (char)bufBytes[startInd + 3];
		if (iCur > 127)
		{
			lRet = iCur & 0x7F;
			lRet += 0x80;
		}
		 else
		{
			 lRet = iCur;
		}
 		lRet *= 256;
 		
		iCur = (char)bufBytes[startInd + 2];
		if (iCur > 127)
		{
			lRet += iCur & 0x7F;
			lRet += 0x80;
		}
		 else
		{
			 lRet += iCur;
		}
 		lRet *= 256;
 		
		iCur = (char)bufBytes[startInd + 1];
		if (iCur > 127)
		{
			lRet += iCur & 0x7F;
			lRet += 0x80;
		}
		 else
		{
			 lRet += iCur;
		}
 		lRet *= 256;

		iCur = (char)bufBytes[startInd];
		if (iCur > 127)
		{
			lRet += iCur & 0x7F;
			lRet += 0x80;
		}
		 else
		{
			 lRet += iCur;
		}
		return lRet;
	}

	//
	// 	Converts a string of hex pairs into a byte array. We know that they
	//	are 16 pairs, but we still return the length just in case for future
	//	uses perhaps.
	//
	private static int stringToBuf(String strData, byte[] bufTar)
	{
		int iLen = strData.length();
		int curValH, curValL;
		
		int outIndex = 0;
		int srcIndex = 0;
		while (srcIndex < iLen)
		{
			curValH = Character.digit(strData.charAt(srcIndex++), 16);
			curValL = Character.digit(strData.charAt(srcIndex++), 16);
			
			curValH *= 16;
			curValH += curValL;
			bufTar[outIndex++] = (byte)curValH;  
		}
		return outIndex;
	}
	
	// If the GW returns an exception, this is called to throw it
	private static void throwCIDErr(Element errElem)
	{
		throw new CIDExcept
		(
			errElem.getTextContent()
			, errElem.getAttribute("CQCGW:File")
			, Integer.parseInt(errElem.getAttribute("CQCGW:Line"))
			, errElem.getAttribute("CQCGW:Process")
			, errElem.getAttribute("CQCGW:Thread")
		);
	}
	
	// Our class members
	private DocumentBuilder docBuilder;
	
	// We'll grow this as required
	private int inBufSz = 1024;
	private byte[] inBuf;

	// private long seqNumIn;
	private long seqNumOut;
	private Socket srvSock;
	private String password;
	private int srvPort;
	private String srvAddr;
	private String userName;
	
    static final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
}
