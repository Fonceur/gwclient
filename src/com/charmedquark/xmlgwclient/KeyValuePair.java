package com.charmedquark.xmlgwclient;

//
// 	Since Java doesn't support a key/value pair, and we have some specialized
//	needs for various lists that track key'd values, we create one of our own.
//	We support a couple string values and an extra index value that will often
//	be extremely useful for our needs.
//
public final class KeyValuePair
{
	public KeyValuePair()
	{
		keyStr = new String();
		valStr = new String();
		valStr2 = new String();
		index = 0;
	}

	public KeyValuePair(String initKey)
	{
		keyStr = new String(initKey);
		valStr = new String();
		valStr2 = new String();
		index = 0;
	}

	public KeyValuePair(String initKey, String initVal)
	{
		keyStr = new String(initKey);
		valStr = new String(initVal);
		valStr2 = new String();
		index = 0;
	}

	public KeyValuePair(String initKey, String initVal, String initVal2)
	{
		keyStr = new String(initKey);
		valStr = new String(initVal);
		valStr2 = new String(initVal2);
		index = 0;
	}

	public int getIndex()
	{
		return index;
	}
	
	public String getKey()
	{
		return keyStr;
	}

	public String getValue()
	{
		return valStr;
	}
	
	public String getValue2()
	{
		return valStr2;
	}

	public void reset()
	{
		keyStr = new String();
		valStr = new String();
		valStr2 = new String();
		index = 0;
	}
	
	public void setIndex(int toSet)
	{
		index = toSet;
	}
	
	public void setKey(String newKey)
	{
		keyStr = newKey;
	}
	
	public void setValue(String newVal)
	{
		valStr = newVal;
	}
	
	public void setValue2(String newVal)
	{
		valStr2 = newVal;
	}

	public void set(String newKey, String newVal)
	{
		keyStr = newKey;
		valStr = newVal;
	}

	public void set(String newKey, String newVal, String newVal2)
	{
		keyStr = newKey;
		valStr = newVal;
		valStr2 = newVal2;
	}
	
	private String keyStr;
	private String valStr;
	private String valStr2;
	private int    index;
}
