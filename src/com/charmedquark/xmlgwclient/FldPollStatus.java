package com.charmedquark.xmlgwclient;

//
//	Returned by the polling method which returns info on all fields that have
//	changed since the last poll. We have to provide the field name, the status,
//	and the value if the status indicates it's valid. And we return the index
//	of this field within the original poll list.
//
public final class FldPollStatus
{
	// The field status
	public enum FldStatus { Unknown, Error, Valid };
	
	public FldPollStatus()
	{
		fldName = new String();
		fldStatus = FldStatus.Unknown;
		fldValue = new String();
		fldIndex = -1;
	}

	//
	// 	A convenience, when we get a field in error so only the name, index and
	//	error status need to be set.
	//
	public FldPollStatus(String name, int index)
	{
		fldName = name;
		fldStatus = FldStatus.Error;
		fldValue = new String();
		fldIndex = index;
	}

	public FldPollStatus(String name, int index, String value)
	{
		fldName = name;
		fldStatus = FldStatus.Valid;
		fldValue = value;
		fldIndex = index;
	}
	
	
	public Boolean getIsValid()
	{
		return (fldStatus == FldStatus.Valid);
	}

	public int getIndex()
	{
		return fldIndex;
	}
	
	public String getName()
	{
		return fldName;
	}
	
	public String getValue()
	{
		return fldValue;
	}
	
	
	private String fldName;
	private FldStatus fldStatus;
	private String fldValue;
	private int fldIndex;
}
